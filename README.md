# GO AST Scanner analyzer

**As of GitLab %11.3, this repository is deprecated, and renamed to [gosec](https://gitlab.com/gitlab-org/security-products/analyzers/gosec/).**

This analyzer is a wrapper around [GO AST Scanner](https://github.com/GoASTScanner/gas),
a tool that inspects source code for security problems by scanning the Go AST.
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
